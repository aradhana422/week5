import java.util.*;
public class Square  extends Rectangle{
	Square(int side){
		super(side, side);
	}

	Square(int length, int breadth) {
		super(length, breadth);	
	}

	public static void main(String[] args) {
		Rectangle r = new Rectangle(5,10);
		r.area();
		r.perimeter();
		Square s = new Square(10);
		s.area();
		s.perimeter();
		Square ss = new Square(6,6);
		ss.area();
		ss.perimeter();

		Square[] squareArray = new Square[10];

		Scanner sc = new Scanner(System.in);
		for (int i =0; i <=9; i++) {
	
			System.out.println("Enter the length of the square.");
			squareArray[i] = new Square(sc.nextInt());
	
			squareArray[i].area();
			squareArray[i].perimeter();
		}	

	}

}
