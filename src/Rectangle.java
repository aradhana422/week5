
public class Rectangle {
	int length;
	int breadth;
	
	
	Rectangle(int length, int breadth){
		this.length = length;
		this.breadth = breadth;
	}
	
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public int getBreadth() {
		return breadth;
	}
	public void setBreadth(int breadth) {
		this.breadth = breadth;
	}

	void area() {
		int area = this.length * this.breadth;
		System.out.println("The area of the object with  lenghth and breadth "+this.length+" and "+ this.breadth + " respectively: "+ area);
	}
	
	void perimeter() {
		int perimeter = 2*(this.length + this.breadth);
		System.out.println("The perimeter of the object is: " + perimeter);
	}


}
