
public class Employee extends Member{
	static String specialization = "Associate";

	public static void main(String[] args) {
		Member m = new Employee();

				m.setName("Ginnele Gellert");
				m.setAge(27);
				m.setPhoneNumber(987564123);
				m.setAddress("803 Ambassador Str");
				m.setSalary(30000);
		
				System.out.println("Name:" + m.name + " Age:" + m.age + " PhoneNo:" +  m.phoneNumber + " Address:" +  m.address + " Specialization:"+ specialization );
				m.printSalary();
		
	}

}
