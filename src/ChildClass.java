
public class ChildClass extends ParentClass {
	int value = 5;
	void print() {
		System.out.println("This is a child class.");
	}

	public static void main(String[] args) {
		ChildClass cc = new ChildClass();
		//Calling method of ChildClass and parent class by object of child class.
				cc.print();
				cc.print1();
				System.out.println(cc.value);
		//Calling method of parent class by object of child class.
				ParentClass p = new ChildClass();
				p.print1();
				System.out.println(p.value);

	


	}

}
